module task
  use mpi
  implicit none
contains
  subroutine GetMaxCoordinates(A, x1, y1, x2, y2)
      implicit none
      real(8), dimension(:,:),intent(in) :: A
      integer(4), intent(out) :: x1, y1, x2, y2
      integer(4) :: n, m, L, R, U, D, transit, minpos, i
      real(8), allocatable :: current_column(:), B(:,:)
      logical :: transp
      real(8) :: current_sum, inter_sum, loc_maxsum,glob_maxsum
      integer(4) :: loc_rank_maxsum,glob_rank_maxsum
      integer(4) :: mpiErr, mpiSize, mpiRank, local_coord(4) 
      
      call mpi_comm_size(MPI_COMM_WORLD, mpiSize, mpiErr)
      
      call mpi_comm_rank(MPI_COMM_WORLD, mpiRank, mpiErr)
      
      m = size(A, dim=1)
      m = size(A, dim=2)
      transp = .FALSE.
      
      if (m < n) then
         transp = .TRUE.
         B = transpose(A)
         m = size(B, 1)
         n = size(B, 2)
      else
         B=A
      endif
      
      allocate(current_column(m))
      local_coord=1
      loc_maxsum=B(1,1)
         
      do L = mpiRank + 1, n, mpiSize
      
         current_column = B(:, L)
         
         do R=L,n
         
            if(R > L) then
               current_column = current_column + B(:, R)
            endif

            current_sum=current_column(1)
            U=1; D=1; inter_sum=0; minpos=0;
 
            do i=1, size(current_column)
                 inter_sum=inter_sum+current_column(i)
            
                 if (inter_sum>current_sum) then
                        current_sum=inter_sum
                        U=minpos+1
                        D=i
                 endif

                 if (inter_sum<0) then
                        inter_sum=0
                        minpos=i
                 endif
            
            enddo
            
            if (current_sum > loc_maxsum) then
             loc_maxsum = current_sum
             local_coord(1)=U
             local_coord(2)=D
             local_coord(3)=L
             local_coord(4)=R

            endif
         
          
         enddo
      enddo
      
      call mpi_allreduce(loc_maxsum, glob_maxsum, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD, mpiErr)
      loc_rank_maxsum=-1

      if (glob_maxsum==loc_maxsum) then
            loc_rank_maxsum=mpiRank
      endif
      
      call mpi_allreduce(loc_rank_maxsum,glob_rank_maxsum,1,MPI_INTEGER4,MPI_MAX,MPI_COMM_WORLD,mpiErr)
      call mpi_bcast(local_coord,4,MPI_INTEGER4,glob_rank_maxsum,MPI_COMM_WORLD,mpiErr)

      deallocate(current_column)

      x1=local_coord(1)
      x2=local_coord(2)
      y1=local_coord(3)
      y2=local_coord(4)

      if (transp) then
            transit=x1
            x1=y1
            y1=transit

            transit=y2
            y2=x2
            x2=transit
      endif
    
  end subroutine GetMaxCoordinates
    
end module task
            
